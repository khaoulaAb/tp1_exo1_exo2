package fr.univangers.masterinfo.ester.webapp.tp.context;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import fr.univangers.masterinfo.ester.webapp.tp.bean.UserBean;
import fr.univangers.masterinfo.ester.webapp.tp.bean.UserFakeData;
import fr.univangers.masterinfo.ester.webapp.tp.dao.UserDaoContract;
import fr.univangers.masterinfo.ester.webapp.tp.utils.SpringUtils;

/**
 * SpringContext
 */
@WebListener
public class SpringContext implements ServletContextListener {

	@Override
	public void contextInitialized(final ServletContextEvent sce) {
		SpringUtils.getInstance();

		// A décommenter pour peupler la BD pour la première fois, après la premier exécution mettez
		// les ligne en commentaire

		final UserDaoContract userDao = SpringUtils.getInstance().getBean(UserDaoContract.class);

		for (final UserBean user : UserFakeData.getListUsers()) {
			userDao.save(user);
		}
	}
	
	@Override
	public void contextDestroyed(final ServletContextEvent sce) {
		SpringUtils.getInstance().close();
	}
}