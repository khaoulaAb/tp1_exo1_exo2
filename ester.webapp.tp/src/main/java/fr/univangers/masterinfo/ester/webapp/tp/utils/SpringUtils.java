package fr.univangers.masterinfo.ester.webapp.tp.utils;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import fr.univangers.masterinfo.ester.webapp.tp.config.SpringConfig;

/**
 * SpringUtils
 */
public class SpringUtils {
	
	/**
	 * Context de Spring qui contient toutes les instances d'objets
	 */
	private static AnnotationConfigApplicationContext context;
	
	/**
	 * Constructeur privé
	 */
	private SpringUtils() {
		
	}
	
	/**
	 * @return l'instance unique du conteneur Spring
	 */
	public static AnnotationConfigApplicationContext getInstance() {
		if (context == null) {
			context = new AnnotationConfigApplicationContext(SpringConfig.class);
		}
		
		return context;
	}
}
