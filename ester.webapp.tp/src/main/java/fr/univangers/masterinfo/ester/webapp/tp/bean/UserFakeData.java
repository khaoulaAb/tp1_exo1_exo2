package fr.univangers.masterinfo.ester.webapp.tp.bean;

import java.util.Arrays;
import java.util.List;

/**
 * UserFakeData
 */
public class UserFakeData {
	
	/**
	 * Constructeur privé
	 */
	private UserFakeData() {

	}

	/**
	 * @return une liste d'utilisateur à enregistrer dans la BD
	 */
	public static List<UserBean> getListUsers() {
		// créer 3 instances de type UserBean
		/*
		 * userAdmin : login : admin pass : admin userDev : login : developpeur pass : developpeur
		 * userDirecteur login : directeur pass : directeur
		 */
		
		final UserBean userAdmin = new UserBean();
		final UserBean userDirecteur = new UserBean();
		final UserBean userDev = new UserBean();

		userDev.setLogin("developpeur");
		userDev.setPassword("developpeur");
		
		userAdmin.setLogin("admin");
		userAdmin.setPassword("admin");
		
		userDirecteur.setLogin("directeur");
		userDirecteur.setPassword("directeur");

		return Arrays.asList(userAdmin, userDev, userDirecteur);
	}
}
