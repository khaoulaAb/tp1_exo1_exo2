package fr.univangers.masterinfo.ester.webapp.tp.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * UserBean
 */
// TODO Annotation marque la classe en tant qu'une entité
@Entity
// TODO Annotation spécifie la table que représente l'entité avec le nom suivant t_user
@Table(name = "t_user")
public class UserBean {
	
	/**
	 * Identifiant unique de l'utilisateur
	 */
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;

	/**
	 * Le login de l'utilisateur
	 */
	// TODO ajouter une annotation qui permet de préciser que cet attribut est une colonne dans la
	// base de données avce le suivant use_login
	// TODO attribut String privé nommé login, puis décommenter la ligne dessus
	@Column(name = "use_login")
	private String login;
	
	/**
	 * Le mot de passe de l'utilisateur
	 */
	// TODO ajouter une annotation qui permet de préciser que cet attribut est une colonne dans la
	// base de données avce le suivant use_password
	// TODO attribut privé String nommé password, puis décommenter la ligne dessus
	@Column(name = "use_password")
	private String password;

	// TODO générer les getter & setter des 2 attributs : login et password
	public String getLogin() {
		return this.login;
	}
	
	public void setLogin(final String login) {
		this.login = login;
	}
	
	public String getPassword() {
		return this.password;
	}
	
	public void setPassword(final String password) {
		this.password = password;
	}
	
}
