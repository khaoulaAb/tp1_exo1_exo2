package fr.univangers.masterinfo.ester.webapp.tp.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.univangers.masterinfo.ester.webapp.tp.bean.UserBean;
import fr.univangers.masterinfo.ester.webapp.tp.dao.UserDaoContract;
import fr.univangers.masterinfo.ester.webapp.tp.utils.SpringUtils;

/**
 * LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	
	/**
	 * Le numéro de série
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Log de la classe
	 */
	private static final Logger LOG = LogManager.getLogger(LoginServlet.class);
	
	/**
	 * La JSP login
	 */
	private static final String LOGIN_JSP = "WEB-INF/login.jsp";
	
	/**
	 * La JSP membre
	 */
	private static final String MEMBER_JSP = "WEB-INF/member.jsp";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServlet() {
		super();
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher(LOGIN_JSP).forward(request, response);
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(final HttpServletRequest request, final HttpServletResponse response)
			throws ServletException, IOException {
		final String login = request.getParameter("name");
		final String password = request.getParameter("password");
		String error = StringUtils.EMPTY;
		
		// Validation du formulaire

		if (StringUtils.isBlank(login)) {
			error += "Le login n'est pas renseigné ! ";
		}
		
		if (StringUtils.isBlank(password)) {
			error += "Le mot de passe n'est pas renseigné ! ";
		}
		
		LOG.debug("Login : " + login);
		LOG.debug("Password : " + password);
		LOG.debug("Error : " + error);

		// Si il y a une erreur dans ce cas on l'affiche à l'utilisateur
		if (StringUtils.isNotBlank(error)) {
			request.setAttribute("error", error.trim());
			request.getRequestDispatcher(LOGIN_JSP).forward(request, response);
			return;
		}
		
		// On récupére la DAO user par son contrat dans Spring
		final UserDaoContract userDao = SpringUtils.getInstance().getBean(UserDaoContract.class);
		
		// TODO existUser boolean qui nous permet de savoir si l'utilisateur existe dans notre base
		// de données ou pas
		// indice utiliser l'objet userDao
		final boolean existUser = userDao.existUser(login);

		LOG.debug("existUser : " + existUser);
		if (existUser) {
			// TODO user objet de type UserBean qui contient l'utilisateur récuperer depuis notre
			// base de sonnées par son login
			// indice utiliser l'objet userDao
			final UserBean user = userDao.findUser(login);
			
			LOG.debug("user : " + user);
			if (StringUtils.equals(user.getPassword(), password)) {
				request.setAttribute("user", user);
				// TODO redirection vers member.jsp
				request.getRequestDispatcher(MEMBER_JSP).forward(request, response);
			} else {
				request.setAttribute("error", "Mot de passe incorrect !");
				// TODO redirection vers member.jsp
				request.getRequestDispatcher(LOGIN_JSP).forward(request, response);
			}
		} else {
			request.setAttribute("error", "L'utilisateur " + login + " n'existe pas !");
			request.getRequestDispatcher(LOGIN_JSP).forward(request, response);
		}
	}
}