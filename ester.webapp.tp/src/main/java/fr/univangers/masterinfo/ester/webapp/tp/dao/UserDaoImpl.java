package fr.univangers.masterinfo.ester.webapp.tp.dao;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import fr.univangers.masterinfo.ester.webapp.tp.bean.UserBean;

/**
 * UserDaoImpl
 */
@Repository
public class UserDaoImpl implements UserDaoContract {
	private static final Logger LOG = LogManager.getLogger(UserDaoImpl.class);

	/**
	 * La session automatiquement injecté par Spring
	 */
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public UserBean findOne(final String id) {
		final Session session = this.sessionFactory.getCurrentSession();
		final Transaction tx = session.beginTransaction();
		final UserBean user = session.find(UserBean.class, id);
		tx.commit();
		return user;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UserBean> findAll() {
		final Session session = this.sessionFactory.getCurrentSession();
		final Transaction tx = session.beginTransaction();
		final List<UserBean> users = session.createQuery("FROM " + UserBean.class.getName()).list();
		tx.commit();
		return users;
	}

	@Override
	public void save(final UserBean user) {
		final Session session = this.sessionFactory.getCurrentSession();
		final Transaction tx = session.beginTransaction();
		session.save(user);
		tx.commit();
	}

	@Override
	public void update(final UserBean user) {
		final Session session = this.sessionFactory.getCurrentSession();
		final Transaction tx = session.beginTransaction();
		session.update(user);
		tx.commit();
	}

	@Override
	public void deleteOne(final UserBean user) {
		final Session session = this.sessionFactory.getCurrentSession();
		final Transaction tx = session.beginTransaction();
		session.remove(user);
		tx.commit();
	}

	@Override
	public void deleteAll() {
		
	}

	@Override
	public boolean existUser(final String login) {
		final Session session = this.sessionFactory.getCurrentSession();
		final Transaction tx = session.beginTransaction();
		
		final StringBuilder sb = new StringBuilder();
		sb.append(" SELECT COUNT(*) ");
		sb.append(" FROM " + UserBean.class.getName() + " AS user ");
		sb.append(" WHERE user.login = :login");
		
		final Query<?> query = session.createQuery(sb.toString());
		query.setParameter("login", login);
		
		final Integer count = (Integer) query.uniqueResult();

		tx.commit();

		return (count != null) && (count > 0);
	}

	@Override
	public UserBean findUser(final String login) {
		final Session session = this.sessionFactory.getCurrentSession();
		final Transaction tx = session.beginTransaction();
		final StringBuilder sb = new StringBuilder();
		
		// TODO en se basant sur le traitement de la fonction existUser
		// écrire la requete HQL qui nous permet de récuperer un utilisateur a partir de son login

		sb.append(" SELECT * ");
		sb.append(" FROM " + UserBean.class.getName() + " AS login");
		sb.append(" WHERE user.login = :login");

		final Query<?> query = session.createQuery(sb.toString());
		query.setParameter("login", login);

		final UserBean userBean = (UserBean) query.uniqueResult();// TODO stocker le resultat
																	// dans userBean
		tx.commit();

		return userBean;

	}

}
