package fr.univangers.masterinfo.ester.webapp.tp.config;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.AvailableSettings;
import org.hibernate.ogm.cfg.OgmConfiguration;
import org.hibernate.ogm.cfg.OgmProperties;
import org.hibernate.service.ServiceRegistry;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import fr.univangers.masterinfo.ester.webapp.tp.bean.UserBean;

/**
 * SpringConfig
 */
@Configuration
@ComponentScan(value = "fr.univangers.masterinfo.ester.webapp.tp")
public class SpringConfig {

	/**
	 * @return la session de MongoDB pour l'interroger
	 */
	
	@SuppressWarnings("static-method")
	@Bean
	public SessionFactory getSessionFactory() {
		
		final OgmConfiguration config = new OgmConfiguration();
		
		config.setProperty(OgmProperties.DATASTORE_PROVIDER, "mongodb"); // TODO
		config.setProperty(OgmProperties.DATABASE, "ester-webapp-tp"); // TODO
		config.setProperty(OgmProperties.CREATE_DATABASE, "true");
		config.setProperty(OgmProperties.HOST, "127.0.0.1:27017"); // TODO
		config.setProperty(AvailableSettings.SHOW_SQL, "true");
		
		// Résout le problème : org.hibernate.HibernateException: No CurrentSessionContext
		// configured!
		config.setProperty(AvailableSettings.CURRENT_SESSION_CONTEXT_CLASS, "thread");
		
		// Les classes entités à ajouter
		config.addAnnotatedClass(UserBean.class);
		
		final ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
				.applySettings(config.getProperties()).build();
		
		return config.buildSessionFactory(serviceRegistry);
	}
}
