<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Connexion</title>
</head>
<body>

	<c:if test="${error}">
		<label style="color:red">le nom ou le mot de passe incorrect</label><br/>			
	</c:if>
	
	<form method="POST" action="LoginServlet">
		<!-- completer le formulaire à fin qu'il resseble à l'image -->
		<h3>Nom: <input type="text" name="name"></h3>
		<h3>Mot de passe: <input type="password" name="password"></h3>
		<input type="submit" value="se connecter">	
	</form>
</body>
</html>