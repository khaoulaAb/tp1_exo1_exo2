package fr.univangers;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ConnexionServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO redirection vers la page login.jsp
		request.getRequestDispatcher("WEB-INF/login.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO 
		/*
		 * 1. Récuperer les parametres envoyer avec le formulaire
		 * 2. Tester si le nom et le mot de passe égale à un nom+mot de passe de ton choix
		 * 3. vrai --> 
		 * 				stocker le nom et le mot de passe dans la requête
		 * 				rediriger les données vers la page member.jsp
		 * 4. faux -->
		 * 				créer un boolen error avec la valeur true
		 * 				stocker le dans la requête
		 * 				rediriger les données vers loogin.jsp
		 */
		String name= request.getParameter("name");
		String password= request.getParameter("password");

		if(name.equals("khaoula") && password.equals("1234")) {
			request.setAttribute("name", name);
			request.setAttribute("password", password);
			request.getRequestDispatcher("WEB-INF/member.jsp").forward(request, response);
		}
		
		else {
			Boolean error = true;
			request.setAttribute("error", error);
			request.getRequestDispatcher("WEB-INF/login.jsp").forward(request, response);
		}
		
		
	}

}
